import java.util.Scanner;

class Program2 {
    public static void main(String[] args) {
        
        System.out.println("Uznayem kolichestvo lokal'nykh minimumov mezhdu chislami");
        System.out.println("Vvedite chisla");
        
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int leftnumber = 0;
        int betweennumber = number;
        int rightnumber = 0;
        int sumlocalmin = 0;
        
        while (number != -1) {
            if (leftnumber > betweennumber && betweennumber < rightnumber) {
                sumlocalmin++;
            }
            leftnumber = betweennumber;
            betweennumber = rightnumber;
            rightnumber = number;
            number = scanner.nextInt(); 
                        
        }
        System.out.println("Kolichestvo lokal'nykh minimumov " + sumlocalmin);
    }
}
