import java.util.Scanner;

class Program {
    public static void main(String[] args) {

        System.out.println("Naydem chislo, u kotorogo summa tsifr minimal'naya sredi vsekh ostal'nykh");
        System.out.println("Vvedite chisla");

        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int digitsSum = 0;
        int min = 0;
        int digit = 0;
        int n = 0;

        while (number != -1) {
            n = number;
            while (number != 0) {
                int lastDigit = number % 10;
                number = number / 10;
                digitsSum = digitsSum + lastDigit;
            }
            if (min == 0) {
                min = digitsSum;
            }
            if (min > digitsSum) {
                min = digitsSum;
                digit = n;
            } 
            digitsSum = 0;
            number = scanner.nextInt();
        }
        System.out.println("Rezul'tat: " + digit);
        System.out.println("Summa chisel: " + min);
    }
}
