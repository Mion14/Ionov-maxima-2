import java.util.Scanner;

class Program {
	public static void main(String[] args) {
    
    	Scanner scanner = new Scanner(System.in);
    	System.out.println("Vvedite dlinu massiva: ");

    	int size = scanner.nextInt(); 
    	int array[] = new int[size]; 
    
    	System.out.println("Zapolnite massiv:");
        
    	for (int i = 0; i < size; i++) {
        	array[i] = scanner.nextInt();
    	}
    		System.out.print("Elementy massiva:");
    		
        for (int i = 0; i < size; i++) {
        	System.out.print(" " + array[i]);
    	}
    		System.out.println();
    		System.out.print("Elementy massiva v obratnom poryadke:");

    	for (int i = size - 1; i >= 0; i--) {
    		System.out.print(array[i] + " "); 
    	}
    }	
}
