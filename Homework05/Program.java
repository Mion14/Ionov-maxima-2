import java.util.Scanner;

public class Program {
    public static void main(String[] args) {

        System.out.println("Поиск часто встречающегося возраста от 0 до 120:");

        int[] array = new int[121];
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        
        
        while (number != -1) {
            array[number] += 1;
            number = scanner.nextInt();
        }
        int maxAgeCount = 0;
        int indexMax = 0;
        
        for (int i = 0; i < array.length; i++) {
            if (array[i] > maxAgeCount) {
                maxAgeCount = array[i];
                indexMax = i;
            }
        }
        System.out.println("Вывод - " + indexMax + " лет, " + "встречается " + maxAgeCount + " раз(а)");
    }
}
